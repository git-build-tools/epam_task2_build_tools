package com.ilyastuit.task1;

import org.apache.commons.lang3.math.NumberUtils;

public class StringUtils {
    public boolean isPositiveNumber(String str) {
        return NumberUtils.isCreatable(str) && NumberUtils.toDouble(str) > 0;
    }
}
