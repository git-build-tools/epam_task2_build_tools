package com.ilyastuit.task1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringUtilsTest {
    private final StringUtils stringUtils = new StringUtils();

    @Test
    public void testIsNull() {
        String str = null;

        Assertions.assertFalse(stringUtils.isPositiveNumber(str));
    }

    @Test
    public void testIsNotNumber() {
        String str = "not a number";

        Assertions.assertFalse(stringUtils.isPositiveNumber(str));
    }

    @Test
    public void testIsNumberAndIsNotPositive() {
        String str = "-2";

        Assertions.assertFalse(stringUtils.isPositiveNumber(str));
    }

    @Test
    public void testIsNumberAndPositive() {
        String str = "10";

        Assertions.assertTrue(stringUtils.isPositiveNumber(str));
    }
}
